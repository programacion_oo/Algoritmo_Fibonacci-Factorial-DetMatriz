/*
 * NOMBRE PROGRAMA:FACTORIAL
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:05/04/18
 */
package Ejercicio1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Factorial {
	public static void main(String Arg[ ]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta;
		do{
			//declarar variables
			int num=0;
			int opcion;
			System.out.print("HALLAR FACTORIAL");
			do{
			System.out.print("\nIngrese el numero que quiere saber el factorial: ");
			//ingresar numero para sacar factorial
			num=Integer.parseInt(in.readLine( ));
			}while(num<=-1);
			
			do{
			//menu de opciones	
			System.out.println(" MENU DE OPCIONES \n");
			System.out.println(" OPCION 1: DE FORMA RECURSIVA ");
			System.out.println(" OPCION 2: DE FORMA ITERATIVA");
			System.out.print("Ingrese el numero : ");
			opcion = Integer.parseInt(in.readLine( ));

			//mostrar aviso de opcion invalida
			if(opcion>3 || opcion<1){
				System.out.println("SELECCIONAR UNA OPCION VALIDA");
			}
			//Retornar al menu de opciones si no se escribe una opcion correcta	
		}while(opcion>3 || opcion<1);
			
			
			switch(opcion){

			case 1:
			{
				//crear el objeto
				Fact fact=new Fact();
				//imprimir resultado
				System.out.print("El factorial de "+num+" es "+fact.Factor(num));	
			}
			break;
			case 2:
			{
				//crear el objeto
				Factor factor=new Factor(num);
				//imprimir resultado
				System.out.print("El factorial de "+num+" es "+factor.getFact(num));
				
			}	
			break;
			}
			System.out.println("�Desea ejecutar el programa de nuevo?(si=1)");
			pregunta = Integer.parseInt(in.readLine( ));
		}while(pregunta==1);	
		System.out.println("Gracias por usar el programa");
	}
}
