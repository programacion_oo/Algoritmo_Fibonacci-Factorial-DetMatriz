package Ejercicio1;

public class Fact {
	private int factor;
	//devolver factorial
	public int Factor(int num){
		//factorial de 0=1
		if(num==0){
			factor=1;
		}
		else{
			//factorial n = n*factorial de n-1
			factor= num*Factor(num-1);
		}
	return factor;
	}
	
	public int getFactor() {
		return factor;
	}
	public void setFactor(int factor) {
		this.factor = factor;
	}
}
