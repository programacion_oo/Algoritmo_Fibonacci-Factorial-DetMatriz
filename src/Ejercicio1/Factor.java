package Ejercicio1;

public class Factor {
	private int fact;
	public Factor(int num){
		int aux=1;
		for (int i = 2; i <= num; i ++){ // hace la operación iterativamente 
		aux *= i; 
		}
		fact=aux;
		}
	
	public int getFact(int num) {
		return fact;
	}
	public void setFact(int fact) {
		this.fact = fact;
	}
}

