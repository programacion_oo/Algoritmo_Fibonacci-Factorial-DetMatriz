package Ejercicio3;
import java.util.Scanner;

public class LlenarMatriz {

	double[][]devolver (double [][]matriz, int fil, int col){
		
		Scanner entrada=new Scanner(System.in);
		if(fil<3 && col<3){
			System.out.print("Digite el numero en la posicion ["+fil+"] ["+col+"]: ");
			matriz[fil][col]=entrada.nextInt();	
			//contar hasta llegar al limite de la matriz
			if(col==2){
				fil++;
				col=0;
			}
			else{
				//seguir en la misma columna
				col++;	
			}
			//reiterar el proceso
			devolver(matriz,fil,col);
		}
		return matriz;
	}
}
