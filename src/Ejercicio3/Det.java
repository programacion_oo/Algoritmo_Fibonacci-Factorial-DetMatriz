package Ejercicio3;
public class Det {
	private double determinante;
public Det(double[][]matriz){
	double d1=matriz[0][0]+matriz[1][1]+matriz[2][2];
	double d2=matriz[0][1]+matriz[1][2]+matriz[2][0];
	double d3=matriz[0][2]+matriz[1][0]+matriz[2][1];
	double d4=matriz[0][2]+matriz[1][1]+matriz[2][0];
	double d5=matriz[0][1]+matriz[1][0]+matriz[2][2];
	double d6=matriz[0][0]+matriz[1][2]+matriz[2][1];
	determinante=d1+d2+d3-(d4+d5+d6);
}
public double getDeterminante() {
	return determinante;
}
public void setDeterminante(double determinante) {
	this.determinante = determinante;
}

}
