/*
 * NOMBRE PROGRAMA:MATRIZ 3X3
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:05/04/18
 */
package Ejercicio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Matriz3x3 {
	public static void main(String Arg[ ]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta=0;
		do{
			System.out.print("LLENAR MATRIZ 3X3\n");
			//declarar variables
			int fil=0;
			int col=0;
			int opcion;
			//crear matriz
			double[][]matriz=new double[3][3];
			//llenar matriz
			LlenarMatriz llenar=new LlenarMatriz();
			//crear matriz auxiliar
			double[][]matrizf=llenar.devolver(matriz, fil, col);
			//imprimir matriz
			for(fil=0;fil<3;fil++){
				for(col=0;col<3;col++){
					System.out.printf("%-7s",matrizf[fil][col]);
				}
				System.out.print("\n");
			}
			do{
				//menu de opciones	
				System.out.println(" MENU DE OPCIONES \n");
				System.out.println(" OPCION 1: DE FORMA RECURSIVA ");
				System.out.println(" OPCION 2: DE FORMA ITERATIVA");
				System.out.print("Ingrese el numero : ");
				opcion = Integer.parseInt(in.readLine( ));

				//mostrar aviso de opcion invalida
				if(opcion>3 || opcion<1){
					System.out.println("SELECCIONAR UNA OPCION VALIDA");
				}
				//Retornar al menu de opciones si no se escribe una opcion correcta	
			}while(opcion>3 || opcion<1);
				
				
				switch(opcion){

				case 1:
				{
					System.out .println("El determinante de la matriz es: "+Determinante.getDeterminante(matriz));
				}
				break;
				case 2:
				{
					Det determinante=new Det(matriz);
					System.out.print("El determinante es "+determinante.getDeterminante());
				}	
				break;
				}
				System.out.println("\n �Desea realizar otro calculo? (Si=1)");
				pregunta = Integer.parseInt(in.readLine( ));
		}while(pregunta==1);
	}
}
