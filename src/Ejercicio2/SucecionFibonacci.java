/*
 * NOMBRE PROGRAMA:SUCECION FIBONACCI
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:05/04/18
 */
package Ejercicio2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SucecionFibonacci {
	public static void main(String Arg[ ]) throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int pregunta=0;
		do{
			int num;
			int opcion;
			System.out.print("SUCECION FIBONACCI");
			do{
			//ingresar dato
			System.out.print("\ningrese el numero que quiere conocer de la sucecion: ");
			num=Integer.parseInt(in.readLine( ));
			}while(num<0);
			
			do{
				//menu de opciones	
				System.out.println(" MENU DE OPCIONES \n");
				System.out.println(" OPCION 1: DE FORMA RECURSIVA ");
				System.out.println(" OPCION 2: DE FORMA ITERATIVA");
				System.out.print("Ingrese el numero : ");
				opcion = Integer.parseInt(in.readLine( ));

				//mostrar aviso de opcion invalida
				if(opcion>3 || opcion<1){
					System.out.println("SELECCIONAR UNA OPCION VALIDA");
				}
				//Retornar al menu de opciones si no se escribe una opcion correcta	
			}while(opcion>3 || opcion<1);
				
				
				switch(opcion){

				case 1:
				{
					//crear objeto
					Fibonacci sucesion=new Fibonacci();
					//imprimir respuesta
					for(int i=0;i<num;i++){
					System.out.print(sucesion.fibonacci(i)+" ");
					}
				}
				break;
				case 2:
				{
					 int n1 = 0;
				     int n2 = 1;
				     System.out.print(n1 + " ");
				     System.out.print(n2 + " ");
				     Fibo fibonacci=new Fibo(num,n1,n2);
				     
				
				}	
				break;
				}
			
			
			do{
				System.out.println("\n �Desea realizar otro calculo? Si(0) No(1)");
				pregunta = Integer.parseInt(in.readLine( ));
			}while(pregunta<0 || pregunta>1);
			//Pregunta si quiere retornar a hacer otro calculo
		}while(pregunta==0);
		if(pregunta!=0)
		{
			System.out.println("FIN DEL PROGRAMA");
		}	
	}
}
