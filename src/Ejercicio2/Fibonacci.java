/*
 * NOMBRE PROGRAMA:SUCECION FIBONACCI
 * AUTOR:Andres Felipe Wilches Torres
 * FECHA:05/04/18
 */
package Ejercicio2;

public class Fibonacci {
	private int suc=0;
	//metodo para sacar fibonacci
	public int fibonacci(int num){
		if(num==1 || num==0){
			return num;
		}
		else{	
			return fibonacci(num-1)+fibonacci(num-2);
		}
	}
}
	